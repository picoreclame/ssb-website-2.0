# ssb-website-2.0

This is a collaborative repo for the redesign and building of [ssb.nz](ssb.nz)


### Rough Design

We have a rough design here on [Figma](https://www.figma.com/file/cncbZdFKTpOEbKYGPjkTczVc/scuttlebutt?node-id=0%3A1).

### Contributing

> Chat, Share, Fork, Code and Merge Request

Most of this gathering, communication, and pair programming is organised from within scuttlebutt. The message id for the convo's below!

Part one: `%90cpJesddE8aullfGxLR70nCRWE7i2F1gkuwZNujRnc=.sha256`
Part two: `%nIAh1T1uzASGMbO2rq+Z1ygWkIj/7EPctqpeCnSh7WU=.sha256`

## Build Record

We're keeping track of what needs to be done, and also time spent. Trying to transparently share and acknowledge people for their care and time.

Part one: [here](Part One of Decentralised Building.md)

## Miscellaneous References

* [localization support](https://github.com/stefalda/localized-strings)
